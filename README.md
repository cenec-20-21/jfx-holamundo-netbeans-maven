# jfx-holamundo-netbeans-maven

Aplicación básica con JFX y NetBeans y Maven para manejar dependencias a librerías JFX (Java FX SDK no requerido)

## Requisitos previos
1. Tener JDK instalado y PATH apuntando a sus binarios (para que se encuentre java y javac)
2. Tener Maven instalado y PATH apuntando a sus binarios
3. Tener Apache NetBeans

## Compilar código
1. Abrir el proyecto en NetBeans o crear uno nuevo como `Simple JavaFX Maven Archetype (Gluon)`
   ![nuevo proyecto maven](img/mavenJfxProject.png)
   ![nombre y versiones](img/holaMundo.png)
2. Compilar 
3. Ejecutar
